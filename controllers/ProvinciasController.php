<?php

namespace app\controllers;

use Yii;
use app\models\Provincias;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;
use app\models\Provincias;

/**
 * ProvinciasController implements the CRUD actions for Provincias model.
 */
class ProvinciasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Provincias models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Provincias::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Provincias model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Provincias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Provincias();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->provincia]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionEjercicio1(){
        $numero = Yii::$app->db
                ->createCommand('SELECT MIN(p.poblacion), C4.provincia FROM (
    SELECT p.provincia FROM provincias p
     WHERE p.autonomia = (
       SELECT autonomia, C2.minsuper FROM (
         SELECT p.autonomia, MIN(p.superficie) AS minsuper FROM provincias p
         GROUP BY p.autonomia
          HAVING p.autonomia = (
            SELECT p.autonomia FROM provincias p
            GROUP BY p.autonomia
            HAVING COUNT(p.provincia)>2
   )
  ) AS C2
  )
  ) AS C4
  INNER JOIN provincias p
  ON p.provincia=C4.provincia')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql' => 'SELECT MIN(p.poblacion), C4.provincia FROM (
    SELECT p.provincia FROM provincias p
     WHERE p.autonomia = (
       SELECT autonomia, C2.minsuper FROM (
         SELECT p.autonomia, MIN(p.superficie) AS minsuper FROM provincias p
         GROUP BY p.autonomia
          HAVING p.autonomia = (
            SELECT p.autonomia FROM provincias p
            GROUP BY p.autonomia
            HAVING COUNT(p.provincia)>2
   )
  ) AS C2
  )
  ) AS C4
  INNER JOIN provincias p
  ON p.provincia=C4.provincia',
            'totalCount' => $numero,
            'pagination' =>[
                'pageSize'=>5
            ] 
        ]);
        
        return $this -> render("resultado",[
           "resultados"=>$dataProvider,
        "campos"=>['provincia', 'MIN(poblacion)'],
            "titulo"=>"Consulta 1",
            "Explicación"=>"Primero buscamos las autonomias con mas de dos provincias, después, vemos la superficie de las autonomias con más de dos provincias. El siguiente paso es sacar la autonomia con menor superficie de la ultima búsqueda. Buscamos las provincias de la autonomia seleccionada. Por ultimo, tomamos la provincia más despoblada y ya tenemos el resultado deseado",
            "sql"=>"SELECT MIN(p.poblacion), C4.provincia FROM (
    SELECT p.provincia FROM provincias p
     WHERE p.autonomia = (
       SELECT autonomia, C2.minsuper FROM (
         SELECT p.autonomia, MIN(p.superficie) AS minsuper FROM provincias p
         GROUP BY p.autonomia
          HAVING p.autonomia = (
            SELECT p.autonomia FROM provincias p
            GROUP BY p.autonomia
            HAVING COUNT(p.provincia)>2
   )
  ) AS C2
  )
  ) AS C4
  INNER JOIN provincias p
  ON p.provincia=C4.provincia",
        ]);
    }

    /**
     * Updates an existing Provincias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->provincia]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Provincias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Provincias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Provincias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Provincias::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
